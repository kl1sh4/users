<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * UserLog component
 */
class UserLogComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function getLoggedUser()
    {
        try {

            $loggedUser = TableRegistry::getTableLocator()->get('Users')->get($this->getController()->getRequest()->getAttribute('identity')->id, [

                'contain' => ['Roles'],

            ]);
            return $loggedUser;
        } catch (Exception $ex) {


            return null;
        }
    }


}
