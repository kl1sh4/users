<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Logout Controller
 *
 */
class LogoutController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();


        $this->Authentication->logout();

        return $this->redirect('/login/');

    }

}
