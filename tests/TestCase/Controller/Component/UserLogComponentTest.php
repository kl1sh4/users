<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\UserLogComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\UserLogComponent Test Case
 */
class UserLogComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Controller\Component\UserLogComponent
     */
    protected $UserLog;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->UserLog = new UserLogComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->UserLog);

        parent::tearDown();
    }
}
